defmodule VerdeGris.FabricadorController do
  use VerdeGris.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
