defmodule VerdeGris.Router do
  use VerdeGris.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", VerdeGris do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/geomaro", GeomaroController, :index
    get "/fabricador", FabricadorController, :index 
  end

  # Other scopes may use custom stacks.
  # scope "/api", VerdeGris do
  #   pipe_through :api
  # end
end
