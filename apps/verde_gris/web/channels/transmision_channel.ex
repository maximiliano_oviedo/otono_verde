defmodule VerdeGris.TransmisionChannel do
  use VerdeGris.Web, :channel
  alias MecanismoTransmision.Cliente
  alias MecanismoTransmision.Fabrica

  def join("game_transmision:" <> usuario_id, _payload, socket) do
    {:ok, "Joined usuario:#{usuario_id}", socket}
  end

  #intercept ["transmision:iniciar_conexion"]
  def handle_in("transmision:iniciar_conexion_cliente", _payload, socket) do
      Cliente.iniciar_conexion(cliente_name(socket.topic), VerdeGris.TransmisionChannel)
      Cliente.verificar_conexion(cliente_name(socket.topic))
      |> probar_conexion(socket)
  end

  def handle_in("transmision:iniciar_conexion_fabrica", _payload, socket) do
      Fabrica.iniciar_conexion(VerdeGris.TransmisionChannel)
      Fabrica.verificar_conexion()
      |> probar_conexion(socket)
  end

  def handle_in("transmision:transmitir_objeto", payload, socket) do
      Cliente.iniciar_transaccion_con_fabrica(cliente_name(socket.topic), payload["objeto"])
      {:noreply, socket}
  end

  def handle_in("transmision:responder_transmision_objeto", payload, socket) do
      Fabrica.responder_transaccion_hacia_el_cliente(payload["index"], payload["objeto"])
      {:noreply, socket}
  end

  def handle_in("prueba:probar_objeto_recibido_fabrica", _payload, socket) do
      Fabrica.obtener_transaccion_generada()
      |> probar_transmision(socket)
  end

  def handle_in("prueba:probar_objeto_respondido_en_cliente", _payload, socket) do
      Cliente.obtener_transaccion_respuesta(cliente_name(socket.topic))
      |> probar_transmision_respuesta(socket)
  end

  intercept ["ping"]
  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  intercept ["shout"]
  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (game_data:lobby).
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  def handle_in("objeto", payload, socket) do
    broadcast socket, "objeto", payload
    {:noreply, socket}
  end

  def handle_in("objeto_fabrica", payload, socket) do
    broadcast socket, "objeto_fabrica", payload
    {:noreply, socket}
  end

  # This is invoked every time a notification is being broadcast
  # to the client. The default implementation is just to push it
  # downstream but one could filter or change the event.
  def handle_out(event, payload, socket) do
    push socket, event, payload
    {:noreply, socket}
  end

  defp probar_conexion({:ok, :conectado}, socket) do
      broadcast socket, "on_conectado", %{"conectado" => true}
      {:reply, {:ok, %{conectado: true}}, socket}
  end

  defp probar_conexion({:error, :sin_conexion}, socket) do
      broadcast socket, "on_conectado", %{"conectado" => false}
      {:reply, {:ok, %{conectado: false}}, socket}
  end

  defp probar_transmision([{_index_cliente, %{"x" => _x, "y" => _y}}], socket) do
      {:reply, {:ok, %{transmision: true}}, socket}
  end

  defp probar_transmision_respuesta([%{"x" => _x, "y" => _y}], socket) do
      {:reply, {:ok, %{respuesta: true}}, socket}
  end

  def broadcast_change(objeto, id) do
      VerdeGris.Endpoint.broadcast("game_transmision:#{id}", "objeto", objeto)
  end

  def broadcast_change_fabrica(index, objeto) do
      VerdeGris.Endpoint.broadcast("game_transmision:fabrica", "objeto_fabrica", %{"index": index, "objeto": objeto})
  end

  defp cliente_name(topic_string) do
    topic_string |> String.split(":") |> Enum.drop(1) |> hd |> String.to_atom
  end
end
