// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "web/static/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/my_app/endpoint.ex":
import {Socket} from "phoenix"
import logica from "./canvas"
import idGenObj from "./id_gen"

logica.canvas.Init( tipoJugador )

let socket = new Socket("/socket", {params: {token: window.userToken}})

socket.connect()
//let channel           = socket.channel("game_data:lobby", {})
let channel = null;
if(tipoJugador === "cliente")
    channel = socket.channel("game_transmision:" + idGenObj.idGen.newId(), { })
else
    channel = socket.channel("game_transmision:fabrica", { })

/// Events Listeners

$("#interfaz").on("click", "#conexionButton", evento =>
{
    if(tipoJugador === "cliente")
        channel.push("transmision:iniciar_conexion_cliente", { })
    else
        channel.push("transmision:iniciar_conexion_fabrica", { })
})

$("#canvasContainer").on("click", "#myCanvas", evento =>
{
    let objMandar = logica.canvas.obtenerNodoAMandar( evento.offsetX, evento.offsetY )
    if(tipoJugador === "cliente")
    {
        logica.canvas.revisarSiHaAtrapadoUnaGallina( objMandar );
        channel.push("transmision:transmitir_objeto", {"objeto": objMandar})
    }
    else
    {
        let indexAResponder = logica.canvas.obtenerIndexAResponder();
        if(indexAResponder < 0)
        {
            return;
        }
        channel.push("transmision:responder_transmision_objeto", {"index": indexAResponder, "objeto": objMandar})
    }
});

$("#interfaz").on("click", "#mandarButton", evento =>
{
    if(tipoJugador === "fabrica") return;
    if(logica.canvas.noSeHaCompletadoLaFigura())
    {
        alert("No se ha completado la figura");
        return;
    }
    let objMandar = logica.canvas.obtenerObjetoAMandar( )

    logica.canvas.mandarFigura()
    channel.push("transmision:transmitir_objeto", {"objeto": objMandar})
})

$("#interfaz").on("click", "#responderButton", evento =>
{
  //channel.push("figura-for-response", "nueva figura")
  let indexAResponder = logica.canvas.obtenerIndexAResponder();
  if(indexAResponder < 0)
  {
      alert("No se ha elegido un cliente al cual responder");
      return;
  }
  let objMandar = logica.canvas.obtenerObjetoAMandar()
  logica.canvas.mandarFigura()
  logica.canvas.responderFigura()
  // Se tiene que mandar siempre un objeto como el valor de la llave "objeto"
  channel.push("transmision:responder_transmision_objeto", {"index": indexAResponder, "objeto": objMandar})
  console.log("On responder", indexAResponder);
  //channel.push("counter:increment")
})


/// Channel events

channel.on("on_conectado", msg => {
    logica.canvas.activar();
})

channel.on("user:entered", msg => {
    console.log("Usuario nuevo: ", msg);
})

channel.on('phx_reply', payload =>
{
  let response = payload["response"]
  if (response.length <= 0)
    return
})

channel.on('figura-data', payload =>
{
  if (payload != undefined)
  {
    logica.canvas.figuraRecibida(payload.nodos)
  }

})

channel.on('shout', payload =>
{
  if (payload != undefined)
  {
      logica.canvas.figuraRecibida(payload.objeto)
  }

})

// broadcast
channel.on('objeto', payload =>
{
  if (payload != undefined)
  {
      logica.canvas.figuraRecibida(payload)
  }

})

channel.on('objeto_fabrica', payload =>
{
  if (payload != undefined)
  {
      logica.canvas.figuraRecibidaFabrica(payload.index, payload.objeto)
  }

})

// Now that you are connected, you can join channels with a topic:
//let channel = socket.channel("game_data:lobby", {})
channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
