//
//
//
// JS Aire de Otoño
// Verde / Gris
//

var JUEGO = JUEGO || {};

JUEGO.canvas = null;
JUEGO.interfaz = null;
JUEGO.senial = null;
JUEGO.deltaTime = 0.0;
JUEGO.prevTime = Date.now();
JUEGO.deAquiSalioLaFigura = false;

JUEGO.tiempoMaxCongelacion = 35000;
JUEGO.gallinas = [];
JUEGO.maxGallinas = 60;
JUEGO.tiempoDeEsperaParaUnaNuevaGallina = 3000;
JUEGO.tiempoAnterior = 0;
JUEGO.activarGallinas = false;
JUEGO.gallinasActivas = 0;
JUEGO.gallinasAtrapadas = 0;

function Initialize( tipoJugador )
{
	JUEGO.canvas = new Lienzo(640, 640);
	JUEGO.canvas.iniciarLienzo();

	JUEGO.interfaz = new Interfaz();
	JUEGO.interfaz.iniciarBotones( tipoJugador );

	//JUEGO.senial = new Senial();
	//JUEGO.senial.prepararNueva();
};

function Lienzo(w, h)
{
	this.lienzo = null;
	this.color = "#";
	this.width = w;
	this.height = h;
	this.context = null;
	this.tickInterval = null;
	this.listaLineas = [[], []];
	this.nodos = [];
	this.nodosRecibidos = [];
	this.lineasRecibidas = [];
	this.nodosMomentaneos = [];
	this.figuraMandada = [];
	this.nodoClick = null;
	this.seApretoNodo = false;
	this.seMando = false;
	this.listaLineasNodos = [];
	this.yaHayNodoParaConectar = false;
	this.puntosDeAcuerdoALoRecibido = 0;
}

Lienzo.prototype.iniciarLienzo = function()
{
    this.getCanvas( );
    this.iniciarGrid(24, 24);
    this.iniciarJuego( );
};

Lienzo.prototype.getCanvas = function()
{
	this.lienzo = document.createElement("canvas");
	this.lienzo.width = this.width;
	this.lienzo.height = this.height;
	this.lienzo.setAttribute('id', 'myCanvas');
	this.context = this.lienzo.getContext("2d");
  	this.lienzo.addEventListener("mousedown", JUEGO.ClickCanvas);
	document.getElementById("canvasContainer").appendChild(this.lienzo);
};

Lienzo.prototype.iniciarGrid = function(numHor, numVert)
{
	var ancho = this.lienzo.width / numHor;
	var alto = this.lienzo.height / numVert;
	this.iniciarGridHorizontal(ancho, numHor);
	this.iniciarGridVertical(alto, numVert);
};

Lienzo.prototype.iniciarGridHorizontal = function(ancho, numero)
{
	for(var i = 0; i < numero; ++i)
	{
		var posX = ancho + (ancho * i);
		var linea = new Linea(posX, 0, posX, this.lienzo.height);
		this.listaLineas[0].push(linea);
	}
};

Lienzo.prototype.iniciarGridVertical = function(alto, numero)
{
	for(var i = 0; i < numero; ++i)
	{
		var posY = alto + (alto * i);
		var linea = new Linea(0, posY, this.lienzo.width, posY);
		this.listaLineas[1].push(linea);
	}
};

Lienzo.prototype.setBackgroundActivo = function( )
{
	document.querySelector('#myCanvas').classList.add('canvas-activo');
}

Lienzo.prototype.probarDibujado = function()
{
	this.context.beginPath();
	this.context.rect(this.x, 40, 50, 50);
	this.context.fillStyle = "#F33";
	this.context.fill();
	this.context.closePath();
};

Lienzo.prototype.iniciarJuego = function()
{
	this.tickInterval = setInterval(JUEGO.tick, 1000/30);
};

Lienzo.prototype.procesarFiguraRecibida = function( objeto )
{
	if(objeto.terminado)
		this.agregarFiguraRecibida( objeto.nodos )
	else
		this.agregarNodoRecibido( objeto.nodo )
}

Lienzo.prototype.agregarFiguraRecibida = function( nodos )
{
	//this.compararNodosRecibidosConLosMandados( nodos );
	this.revisarSiHaAtrapadoAlgunaGallina( nodos );
	this.nodosRecibidos = [];
	this.lineasRecibidas = [];
	for(var i = 0; i < nodos.length; ++i)
	{
		let nodo = nodos[i];
		let punto = new Nodo(nodo.x, nodo.y)
		punto.color = '#3FE';
		punto.tiempoDibujo = Date.now();
		punto.activo = true;
		this.nodosRecibidos.push(punto);
		if(i <= 0) continue;
		this.formarLineasEntreNodos( i - 1, i );
	};

	this.formarLineasEntreNodos( 0, this.nodosRecibidos.length - 1);
}

Lienzo.prototype.compararNodosMandadosConLosRecibidos = function( nodos )
{
	var nodosMandados = this.figuraMandada;
	var self = this;
	var nodosDistancia = nodos.map(function( nodo )
	{
		var puntoMasCercano = null;
		var distanciaMasBaja = 10000000;
		for(var i = 0; i < nodosMandados.length; ++i)
		{
			var mandado = nodosMandados[i];
			var dist = self.obtenerDistanciaEntrePuntos(nodo.x, nodo.y, mandado.x, mandado.y);
			if(dist <= distanciaMasBaja)
			{
				distanciaMasBaja = dist;
				puntoMasCercano = mandado;
			}
		}

		console.log("puntoMasCercanoCon: " + nodo.x + " - " + nodo.y, puntoMasCercano);
		// Verificar que el nodo mandado pueda atrapar a alguna gallina
		var diferenciaDePuntosEnX = puntoMasCercano.x - nodo.x;
		if(diferenciaDePuntosEnX >= 40) diferenciaDePuntosEnX = 0;
		var diferenciaDePuntosEnY = puntoMasCercano.y - nodo.y;
		if(diferenciaDePuntosEnY >= 40) diferenciaDePuntosEnY = 0;
		self.puntosDeAcuerdoALoRecibido += diferenciaDePuntosEnY + diferenciaDePuntosEnX;
	});

	if(nodos.length === nodosMandados.length)
		this.puntosDeAcuerdoALoRecibido += 100;
	else
		this.puntosDeAcuerdoALoRecibido -= 10;

	console.log("Diferencia de puntos: ", this.puntosDeAcuerdoALoRecibido);
	var texto = document.querySelector( "#puntuacionText" );
	texto.innerHTML = String(this.puntosDeAcuerdoALoRecibido);
}

Lienzo.prototype.revisarSiHaAtrapadoAlgunaGallina = function( nodos )
{
	var self = this;
	JUEGO.gallinas.forEach(function( gallina )
	{
		for(var i = 0; i < nodos.length; ++i)
		{
			var nodo = nodos[i];
			var dist = self.obtenerDistanciaEntrePuntos(gallina.deltaX, gallina.deltaY, nodo.x, nodo.y);
			if(dist <= 60)
			{
				gallina.atrapar();
				self.actualizarPuntajeDeGallinas();
			}
		}
	});
}

Lienzo.prototype.actualizarPuntajeDeGallinas = function( )
{
	var puntaje = document.querySelector( "#puntuacionText" );
	puntaje.innerHTML = JUEGO.gallinasAtrapadas;
}

Lienzo.prototype.formarLineasEntreNodos = function( indexUno, indexDos )
{
	var nodoUno = this.nodosRecibidos[ indexUno ];
	var nodoDos = this.nodosRecibidos[ indexDos ];

	var lineaNueva = new Linea(nodoUno.x, nodoUno.y, nodoDos.x, nodoDos.y);
	this.lineasRecibidas.push(lineaNueva);
};

Lienzo.prototype.agregarNodoRecibido = function( nodo )
{
	let objNodo = { }
	objNodo.tiempoInicial = Date.now()
	objNodo.pintar = true;
	objNodo.nodo = new Nodo(nodo.x, nodo.y)
	objNodo.nodo.color = '#3A7';
	this.nodosMomentaneos.push( objNodo );
}

Lienzo.prototype.agregarFiguraRecibidaFabrica = function( objeto )
{
	if(!objeto.terminado)
	{
		console.log(objeto.nodo);
		let nodo = objeto.nodo;
		let punto = new Nodo(nodo.x, nodo.y)
		punto.color = '#397';
		this.nodosRecibidos.push(punto);
	}
	else
	{
		for(var i = 0; i < datos.length; ++i)
		{
			let nodo = datos[i];
			let punto = new Nodo(nodo.x, nodo.y)
			punto.color = '#5CE';
			this.nodosRecibidos.push(punto);
		}
	}
}

Lienzo.prototype.limpiarPantalla = function()
{
	this.context.clearRect(0, 0, this.lienzo.width, this.lienzo.height);
};

Lienzo.prototype.dibujarMandadoSuccess = function()
{
	if(!this.seMando) return;

	this.context.beginPath();
	this.context.rect(0, 0, this.lienzo.width, this.lienzo.height);
	this.context.fillStyle = "#333";
	this.context.fill();
	this.context.closePath();

	this.seMando = false;
};

Lienzo.prototype.dibujar = function(deltaTime)
{
	this.limpiarPantalla();
	this.dibujarGrid();
	this.dibujarMandadoSuccess();
	this.dibujarLineasEntreNodos();
	this.dibujarNodos();
	this.dibujarNodosMomentaneos();
	this.dibujarFiguraRecibida();

	if(JUEGO.activarGallinas)
		this.dibujarGallinas( );
};

Lienzo.prototype.dibujarGrid = function()
{
	this.dibujarLineasHorizontales();
	this.dibujarLineasVerticales();
};

Lienzo.prototype.dibujarLineasHorizontales = function()
{
	for(var i = 0; i < this.listaLineas[1].length; ++i)
	{
		var linea = this.listaLineas[1][i];
		this.dibujarLinea(linea);
	}
};

Lienzo.prototype.dibujarLineasVerticales = function()
{
	for(var i = 0; i < this.listaLineas[0].length; ++i)
	{
		var linea = this.listaLineas[0][i];
		this.dibujarLinea(linea);
	}
};

Lienzo.prototype.dibujarLinea = function(linea)
{
	this.context.beginPath();
	this.context.moveTo(linea.x1, linea.y1);
	this.context.lineTo(linea.x2, linea.y2);
	this.context.strokeStyle = "#555";
	this.context.lineWidth = .4;
	this.context.stroke();
	this.context.closePath();
};

Lienzo.prototype.dibujarNodosMomentaneos = function( )
{
	let ctx = this.context;
	this.nodosMomentaneos.forEach(function( nodoObj )
	{
		var tiempoEnDibujado = Date.now() - nodoObj.tiempoInicial;
		if(tiempoEnDibujado > 3000) return;

		nodoObj.pintar = !nodoObj.pintar;

		if(nodoObj.pintar)
			nodoObj.nodo.dibujar(ctx);
	});
}

Lienzo.prototype.dibujarNodos = function()
{
	for(var i = 0; i < this.nodos.length; ++i)
		this.nodos[i].dibujar(this.context);
};

Lienzo.prototype.dibujarLineasEntreNodos = function()
{
	for(var i = 0; i < this.listaLineasNodos.length; ++i)
		this.dibujarArista(this.listaLineasNodos[i]);
};

Lienzo.prototype.dibujarArista = function(linea)
{
	this.context.beginPath();
	this.context.moveTo(linea.x1, linea.y1);
	this.context.lineTo(linea.x2, linea.y2);
	this.context.strokeStyle = "#7EA";
	this.context.lineWidth = 5;
	this.context.stroke();
	this.context.closePath();
};

Lienzo.prototype.dibujarFiguraRecibida = function()
{
	for(var i = 0; i < this.nodosRecibidos.length; ++i)
	{
		if(!this.nodosRecibidos[i].activo) continue;
		var tiempoTranscurrido = Date.now() - this.nodosRecibidos[i].tiempoDibujo;
		if(tiempoTranscurrido >= 2000)
		{
			this.nodosRecibidos[i].activo = false;
			this.nodosRecibidos[i].tiempoDibujo = 0;
		}

		this.nodosRecibidos[i].dibujar(this.context);
		this.dibujarArista(this.lineasRecibidas[i]);
	}
}

Lienzo.prototype.dibujarLineasRecibidas = function()
{
	for(var i = 0; i < this.lineasRecibidas.length; ++i)
		this.dibujarArista(this.lineasRecibidas[i]);
};

Lienzo.prototype.dibujarGallinas = function( )
{
	var ctx = this.context;
	JUEGO.gallinas.forEach(function( gallina )
	{
		gallina.dibujar( ctx );
	})
}

Lienzo.prototype.obtenerUnaPosicionEnXDiferente = function( posX )
{
	return getRandomArbitrary(-this.width, this.width * 2);
}

Lienzo.prototype.obtenerUnaPosicionEnYDiferente = function( posY )
{
	return getRandomArbitrary(-this.height, this.height * 2);
}


// Mozilla example
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function Gallina(puntoUno, puntoDos, vel)
{
	this.xUno = puntoUno.x;
	this.yUno = puntoUno.y;
	this.xDos = puntoDos.x;
	this.yDos = puntoDos.y;
	this.deltaX = puntoUno.x;
	this.deltaY = puntoUno.y;
	this.vel = vel;
	this.tiempoCongelacion = 0;
	this.tiempoAtrapado = 0;
	this.congelar = false;
	this.estaAtrapada = false;

	JUEGO.gallinasActivas += 1;

	console.log('Gallina nueva: ', JUEGO.gallinasActivas);
}

Gallina.prototype.tick = function( dt )
{
	if(this.estaAtrapada) return;
	if(this.congelar)
	{
		var tiempo = Date.now() - this.tiempoCongelacion;
		if(tiempo >= JUEGO.tiempoMaxCongelacion)
		{
			this.congelar = false;
			this.tiempoCongelacion = 0;
		}

		return;
	}

	var vDirectorX = this.xDos - this.xUno;
	var vDirectorY = this.yDos - this.yUno;

	var vDirector = { x: vDirectorX, y: vDirectorY };

	var modulo = obtenerModuloDelVector(vDirector);
	var vUnitario = obtenerVectorUnitario(modulo, vDirector);
    this.deltaX += vDirector.x * dt * (this.vel);
    this.deltaY += vDirector.y * dt * (this.vel);

	this.revisarSiNoHaTopadoConPared( );
}

Gallina.prototype.revisarSiNoHaTopadoConPared = function( )
{
	if(this.deltaX > JUEGO.canvas.lienzo.width)
	{
		this.reboteEnX(JUEGO.canvas.lienzo.width);
	}
	else if(this.deltaX < 0)
	{
		this.reboteEnX( 0 );
	}
	else if(this.deltaY > JUEGO.canvas.lienzo.height)
	{
		this.reboteEnY( JUEGO.canvas.lienzo.height );
	}
	else if(this.deltaY < 0)
	{
		this.reboteEnY( 0 );
	}
}

Gallina.prototype.reboteEnX = function( posX )
{
	this.xUno = posX;
	this.yUno = this.deltaY;

	//this.deltaX = 0;
	//this.deltaY = 0;

	if(posX > 0)
		this.xDos = 0;
	else
		this.xDos = JUEGO.canvas.width;
	this.yDos = JUEGO.canvas.obtenerUnaPosicionEnYDiferente( 0 );
}

Gallina.prototype.reboteEnY = function( posY )
{
	this.xUno = this.deltaX;
	this.yUno = posY;

	//this.deltaX = 0;
	//this.deltaY = 0;

	this.xDos = JUEGO.canvas.obtenerUnaPosicionEnXDiferente( 0 );
	if(posY > 0)
		this.yDos = 0;
	else
		this.yDos = JUEGO.canvas.height;
}

Gallina.prototype.dibujar = function( ctx )
{
	ctx.beginPath();
	ctx.arc(this.deltaX, this.deltaY , 3, 0, 2 * Math.PI);
	ctx.lineWidth = 1;
	ctx.strokeStyle = "#333";
	ctx.stroke();
	if(this.estaAtrapada)
		ctx.fillStyle = "#EE9";
	else
		ctx.fillStyle = "#E96";
  	ctx.fill();
	ctx.closePath();
}

Gallina.prototype.iniciarCongelacion = function( )
{
	this.tiempoCongelacion = Date.now();
	this.congelar = true;
}

Gallina.prototype.atrapar = function( )
{
	JUEGO.gallinasActivas -= 1;
	JUEGO.gallinasAtrapadas += 1;
	this.estaAtrapada = true;
	this.tiempoAtrapado = Date.now();
	console.log('Gallina atrapada: ', JUEGO.gallinasActivas);
}

function obtenerModuloDelVector(vector)
{
	var x = vector.x * vector.x;
	var y = vector.y * vector.y;

	var modulo = Math.sqrt(x + y);
	return modulo;
}

function obtenerVectorUnitario(modulo, vector)
{
	return {
	  	x: vector.x / modulo,
		y: vector.y / modulo
	}
}

function Linea(x1, y1, x2, y2)
{
	this.x1 = x1;
	this.y1 = y1;

	this.x2 = x2;
	this.y2 = y2;
}

JUEGO.ClickCanvas = event =>
{
	JUEGO.canvas.OnMouseDown(event);
};

Lienzo.prototype.OnMouseDown = function(event)
{
	if(!this.estaHaciendoClickEnNodo(event.layerX, event.layerY))
		this.crearNodoNuevo(event.layerX, event.layerY);
};

Lienzo.prototype.crearNodoNuevo = function(x, y)
{
	var nodo = new Nodo(x, y);
	this.nodos.push(nodo);
	this.revisarSiHayNodoParaConectar();
};

Lienzo.prototype.revisarSiHayNodoParaConectar = function()
{
	if(this.yaHayNodoParaConectar)
		this.hacerConexionEntreNodos();
	else
		this.yaHayNodoParaConectar = true;
};

Lienzo.prototype.hacerConexionEntreNodos = function()
{
	if(this.nodos.length < 2) return;

	var nodoUno = this.nodos[this.nodos.length - 2];
	var nodoDos = this.nodos[this.nodos.length - 1];

	var lineaNueva = new Linea(nodoUno.x, nodoUno.y, nodoDos.x, nodoDos.y);
	this.listaLineasNodos.push(lineaNueva);
};

Lienzo.prototype.estaHaciendoClickEnNodo = function(x, y)
{
	if(this.nodos.length <= 0) return false;

	for(var i = 0; i < this.nodos.length; ++i)
	{
		if(this.estaElClickLoSuficientementeCerca(x, y, this.nodos[i]))
			return true;
	}

	return false;
};

Lienzo.prototype.estaElClickLoSuficientementeCerca = function(x, y, nodo)
{
	var distancia = this.obtenerDistanciaEntrePuntos(x, y, nodo.x, nodo.y);

	if(distancia <= nodo.radio)
		return this.elClickEstaDentroDelNodo(nodo);

	return false;
};

Lienzo.prototype.obtenerDistanciaEntrePuntos = function(x1, y1, x2, y2)
{
	var x = x2 - x1;
	x = (x * x);

	var y = y2 - y1;
	y = (y * y);

	var d = Math.sqrt(x + y);

	return d;
};

Lienzo.prototype.elClickEstaDentroDelNodo = function(nodo)
{
	//if(this.seApretoNodo)
	this.agregarLineaEntreNodos(nodo);

	return true;
};

Lienzo.prototype.agregarLineaEntreNodos = function(nodo)
{
	var nodoDos = this.nodos[this.nodos.length - 1];
	var linea = new Linea(nodoDos.x, nodoDos.y, nodo.x, nodo.y);
	this.listaLineasNodos.push(linea);
	//this.nodoClick.resetearColor();
	this.seApretoNodo = false;
	this.yaHayNodoParaConectar = false;
	console.log("Terminar figura");
};

Lienzo.prototype.agregarNodoAEspera = function(nodo)
{
	nodo.color = "#F31";
	this.nodoClick = nodo;
	this.seApretoNodo = true;
};

Lienzo.prototype.borrarNodosYLineasEntreEllas = function()
{
	this.nodos = [];
	this.listaLineasNodos = [];
};

function Nodo(x, y)
{
	this.x = x;
	this.y = y;
	this.radio = 5;
	this.color = "#679";
	this.normalColor = "#E73";
};

Nodo.prototype.dibujar = function(context)
{
	context.beginPath();
	context.arc(this.x, this.y , this.radio, 0, 2 * Math.PI);
	context.lineWidth=1;
	context.strokeStyle = this.color;
	context.stroke();
	context.fillStyle = this.color;
  	context.fill();
	context.closePath();
};

Nodo.prototype.resetearColor = function()
{
	this.color = this.normalColor;
};

JUEGO.tick = function()
{
	JUEGO.clock();
	JUEGO.canvas.dibujar(JUEGO.deltaTime);
	FABRICA.tick(JUEGO.deltaTime);

	if(!JUEGO.activarGallinas) return;
	JUEGO.revisarSiHayQueSumarUnaGallinaAlGallinero( JUEGO.deltaTime );
	JUEGO.gallinas.forEach(function( gallina )
	{
		gallina.tick( JUEGO.deltaTime );
	})
};

JUEGO.clock = function()
{
	var now = Date.now();
  	JUEGO.deltaTime = (now - this.prevTime) / 1000;
  	JUEGO.prevTime = now;
};

JUEGO.revisarSiHayQueSumarUnaGallinaAlGallinero = function( dt )
{
	var tiempoParaNuevaGallina = Date.now() - JUEGO.tiempoAnterior;
	if(tiempoParaNuevaGallina >= JUEGO.tiempoDeEsperaParaUnaNuevaGallina)
	{
		if(JUEGO.gallinasActivas >= JUEGO.maxGallinas) return;
		JUEGO.formarUnaNuevaGallina();
		JUEGO.tiempoAnterior = Date.now();
	}
}

JUEGO.formarUnaNuevaGallina = function( )
{
	var puntoUno = getStartPositionGallina();
	var puntoDos = {x: getRandomArbitrary(0, JUEGO.canvas.width), y: getRandomArbitrary(0, JUEGO.canvas.height)};
	JUEGO.gallinas.push(new Gallina( puntoUno, puntoDos, getRandomArbitrary(0.08, 0.18) ) );
}

var getStartPositionGallina = function( )
{
	var punto = {x: 0, y: 0};
	var num = getRandomArbitrary(-2, 2);
	if(num < -1)
	{
		punto.x = JUEGO.canvas.width / 2;
		punto.y = 0;
	}
	else if(num >= -1 && num < 0)
	{
		punto.x = JUEGO.canvas.width / 2;
		punto.y = JUEGO.canvas.height;
	}
	else if(num >= 0 && num < 1)
	{
		punto.x = 0;
		punto.y = JUEGO.canvas.height / 2;
	}
	else if(num >= 1)
	{
		punto.x = JUEGO.canvas.width;
		punto.y = JUEGO.canvas.height / 2;
	}

	return punto;
}

function Interfaz()
{
	this.mandarButton = null;
	this.borrarButton = null;
	this.conexionButton = null;
	this.respuestaButton = null;

	this.cantidad = 0;
	this.cantidadASumar = 1000.0;
}

Interfaz.prototype.iniciarBotones = function( tipoJugador )
{
	this.iniciarConexionButton();
	this.iniciarBorrarButton();

	if(tipoJugador === "fabrica")
		this.iniciarRespuestaButton();
	else
		this.iniciarMandarButton();
};

Interfaz.prototype.iniciarMandarButton = function()
{
	this.mandarButton = document.createElement("BUTTON");
	var texto = document.createTextNode("MANDAR");
	this.mandarButton.appendChild(texto);
	this.mandarButton.setAttribute('id', "mandarButton");

	//this.mandarButton.addEventListener("click", JUEGO.interfaz.mandarButtonClick);
	document.getElementById("interfaz").appendChild(this.mandarButton);
};

Interfaz.prototype.iniciarBorrarButton = function()
{
	this.borrarButton = document.createElement("BUTTON");
	var texto = document.createTextNode("BORRAR");
	this.borrarButton.appendChild(texto);
	this.borrarButton.addEventListener("click", JUEGO.interfaz.borrarButtonClick);
	document.getElementById("interfaz").appendChild(this.borrarButton);
};

Interfaz.prototype.iniciarConexionButton = function()
{
	this.conexionButton = document.createElement("BUTTON");
	var texto = document.createTextNode("CONECTAR");
	this.conexionButton.appendChild(texto);
	this.conexionButton.setAttribute('id', "conexionButton");
	//this.conexionButton.addEventListener("click", JUEGO.interfaz.conexionButtonClick);
	document.getElementById("interfaz").appendChild(this.conexionButton);
};

Interfaz.prototype.iniciarRespuestaButton = function( )
{
	this.responderButton = document.createElement("BUTTON");
	var texto = document.createTextNode("RESPONDER");
	this.responderButton.appendChild(texto);
	this.responderButton.setAttribute('id', "responderButton");
	//this.responderButton.addEventListener("click", JUEGO.interfaz.responderButtonClick);
	document.getElementById("interfaz").appendChild(this.responderButton);
}

Interfaz.prototype.mandarButtonClick = function()
{
	console.log("mandar figura");
	JUEGO.canvas.seMando = true;
	//JUEGO.interfaz.revisarSiSeCompletoFigura();
	//JUEGO.senial.prepararNueva();
	JUEGO.canvas.borrarNodosYLineasEntreEllas();
};

Interfaz.prototype.revisarSiSeCompletoFigura = function()
{
	var numNodos = JUEGO.canvas.nodos.length;
	var numLineas = JUEGO.canvas.listaLineasNodos.length;
	console.log(numNodos + "   -   " + numLineas);
	if(numNodos > 0 && numNodos == numLineas)
		return true;

	return false;
		//JUEGO.interfaz.sumarCantidad();
};

Interfaz.prototype.borrarButtonClick = function()
{
	JUEGO.canvas.borrarNodosYLineasEntreEllas();
};

Interfaz.prototype.conexionButtonClick = function()
{
	console.log('Conexion click');
};

Interfaz.prototype.sumarCantidad = function()
{
	var score = document.getElementById("score");
	this.cantidad += this.cantidadASumar;
	score.innerHTML = this.cantidad;
};

function Rectangulo(x, y, w, h)
{
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;

	this.numPuntoADibujar = -1;
	this.puntos = [];
}

Rectangulo.prototype.Init = function()
{
	for(var i = 0; i < 4; ++i)
		this.sumarNuevoPunto();
};

Rectangulo.prototype.sumarNuevoPunto = function()
{
	var punto = new Nodo(0, 0);
	punto.color = this.color;
	this.puntos.push(punto);
};

Rectangulo.prototype.actualizarPuntos = function()
{
	for(var i = 0; i < this.puntos.length; ++i)
		this.actualizarElPunto(this.puntos[i], i);
};

Rectangulo.prototype.actualizarElPunto = function(punto, index)
{
	if(index == 0)
	{
		punto.x = this.x;
		punto.y = this.y;
	}
	if(index == 1)
	{
		punto.x = this.x + this.w;
		punto.y = this.y;
	}
	if(index == 2)
	{
		punto.x = this.x + this.w;
		punto.y = this.y + this.h;
	}
	if(index == 3)
	{
		punto.x = this.x;
		punto.y = this.y + this.h;
	}
};

Rectangulo.prototype.resetearValores = function()
{
	this.numPuntoADibujar = -1;
};

Rectangulo.prototype.dibujar = function(context)
{
	context.beginPath();
	context.rect(this.x, this.y, this.w, this.h);
    context.fill();
	context.closePath();
};

Rectangulo.prototype.marcarNuevoPunto = function()
{
	++this.numPuntoADibujar;
	if(this.numPuntoADibujar >= this.puntos.length)
		this.numPuntoADibujar = 0;
};

Rectangulo.prototype.dibujarSiguientePunto = function(context)
{
	var punto = this.puntos[this.numPuntoADibujar];
	punto.dibujar(context);
};

function Senial()
{
	this.color = "#1AF";

	this.rect = new Rectangulo(0, 0, 0, 0);
	this.rect.Init();

	this.laSenialEstaEnEspera = false;
	this.sePuedeDibujar = false;
	this.seEstaDibujandoUnPunto = false;

	this.time = 0;
	this.timeADibujarPunto = 0;
}

Senial.prototype.prepararNueva = function()
{
	this.resetearValores();
	setTimeout(RANDOM.NuevaSenial, 1000);
};

Senial.prototype.resetearValores = function()
{
	this.sePuedeDibujar = false;
	this.laSenialEstaEnEspera = false;
	this.seEstaDibujandoUnPunto = false;

	this.time = 0;
	this.timeADibujarPunto = 0;

	this.rect.resetearValores();
};

Senial.prototype.nuevaSenial = function()
{
	this.rect.w = RANDOM.obtenerNuevo(350, 50);
	this.rect.h = RANDOM.obtenerNuevo(350, 50);

	this.rect.x = RANDOM.obtenerNuevo((JUEGO.canvas.width - this.rect.w), 1);
	this.rect.y = RANDOM.obtenerNuevo((JUEGO.canvas.height - this.rect.h), 1);

	this.rect.actualizarPuntos();

	this.laSenialEstaEnEspera = true;
	this.sePuedeDibujar = true;
	this.time = 0;
};

Senial.prototype.dibujar = function(context, deltaTime)
{
	if(!this.sePuedeDibujar) return;

	if(this.seEstaDibujandoUnPunto)
		this.dibujarSiguientePuntoDelRectangulo(context, deltaTime);
	else
		this.time += deltaTime;

	if(!this.seEstaDibujandoUnPunto && this.time >= 0.5)
		this.iniciarDibujoDePunto();
};

Senial.prototype.iniciarDibujoDePunto = function()
{
	this.seEstaDibujandoUnPunto = true;
	this.rect.marcarNuevoPunto();
};

Senial.prototype.dibujarSiguientePuntoDelRectangulo = function(context, deltaTime)
{
	context.fillStyle = this.color;
	this.rect.dibujarSiguientePunto(context);

	if(this.timeADibujarPunto > 0.3)
		this.terminarDeDibujarPunto();

	this.timeADibujarPunto += deltaTime;
};

Senial.prototype.terminarDeDibujarPunto = function()
{
	this.seEstaDibujandoUnPunto = false;
	this.timeADibujarPunto = 0;
	this.time = 0;
};

function LienzoCliente(w, h, id)
{
	this.id = id;
	this.width = w;
	this.height = h;
	this.lienzo = null;
	this.color = "#7F7F7F";
	this.context = null;
	this.activo = false;
	this.nodosFigura = [];
	this.nodosAEliminar = [];
	this.listaLineas = [[], []];
	this.listaLineasNodos = [];
	this.flash = {valor: false, tiempo: 0}

	this.initCanvas( );
	this.iniciarGrid( );
}

LienzoCliente.prototype.initCanvas = function( )
{
	this.lienzo = document.createElement("canvas");
	this.lienzo.width = this.width;
	this.lienzo.height = this.height;
	this.lienzo.setAttribute('id', 'canvas_' + this.id);
	this.lienzo.classList.add('canvas_cliente');
	this.lienzo.addEventListener('click', FABRICA.OnLienzoClienteClick)
	this.context = this.lienzo.getContext("2d");
	var divCanvas = document.createElement("div");
	divCanvas.classList.add('canvas-section');
	var titulo = document.createElement("h5");
	titulo.innerHTML = "Cliente " + this.id;
	divCanvas.appendChild(titulo);
	divCanvas.appendChild(this.lienzo);
	document.getElementById("canvasClientesContainer").appendChild(divCanvas);
}

LienzoCliente.prototype.iniciarGrid = function( )
{
	var ancho = this.lienzo.width / 12;
	var alto = this.lienzo.height / 12;
	this.iniciarGridHorizontal(ancho, 12);
	this.iniciarGridVertical(alto, 12);
}

LienzoCliente.prototype.iniciarGridHorizontal = function(ancho, numero)
{
	for(var i = 0; i < numero; ++i)
	{
		var posX = ancho + (ancho * i);
		var linea = new Linea(posX, 0, posX, this.lienzo.height);
		this.listaLineas[0].push(linea);
	}
};

LienzoCliente.prototype.iniciarGridVertical = function(alto, numero)
{
	for(var i = 0; i < numero; ++i)
	{
		var posY = alto + (alto * i);
		var linea = new Linea(0, posY, this.lienzo.width, posY);
		this.listaLineas[1].push(linea);
	}
};

LienzoCliente.prototype.setActivo = function( activar )
{
	this.activo = activar;
}

LienzoCliente.prototype.actualizarDibujado = function( dt )
{
	this.context.clearRect(0, 0, this.width, this.height);
	this.dibujarComoActivo();
	this.dibujarFlashEnPantalla( dt );
	this.dibujarGrid( );
	let ctx = this.context;
	this.nodosAEliminar.forEach(function( nodoObj )
	{
		var tiempoEnDibujado = Date.now() - nodoObj.tiempoInicial;
		if(tiempoEnDibujado > 3000) return;

		nodoObj.pintar = !nodoObj.pintar;

		if(nodoObj.pintar)
			nodoObj.nodo.dibujar(ctx);
	});

	if(this.nodosFigura.length)
	{
		this.dibujarLineasEntreNodos();

		this.nodosFigura.forEach(function( nodo )
		{
			nodo.dibujar(ctx);
		});

	}

}

LienzoCliente.prototype.dibujarComoActivo = function( )
{
	if(!this.activo) return;

	this.context.beginPath();
	this.context.rect(0, 0, this.width, this.height);
	this.context.fillStyle = "#71F8E5";
	this.context.fill();
	this.context.closePath();
}

LienzoCliente.prototype.dibujarGrid = function()
{
	this.dibujarLineasHorizontales();
	this.dibujarLineasVerticales();
};

LienzoCliente.prototype.dibujarLineasHorizontales = function()
{
	for(var i = 0; i < this.listaLineas[1].length; ++i)
	{
		var linea = this.listaLineas[1][i];
		this.dibujarLinea(linea);
	}
};

LienzoCliente.prototype.dibujarLineasVerticales = function()
{
	for(var i = 0; i < this.listaLineas[0].length; ++i)
	{
		var linea = this.listaLineas[0][i];
		this.dibujarLinea(linea);
	}
}

LienzoCliente.prototype.dibujarLinea = function(linea)
{
	this.context.beginPath();
	this.context.moveTo(linea.x1, linea.y1);
	this.context.lineTo(linea.x2, linea.y2);
	this.context.strokeStyle = "#555";
	this.context.lineWidth = .4;
	this.context.stroke();
	this.context.closePath();
};

LienzoCliente.prototype.dibujarLineasEntreNodos = function()
{
	for(var i = 0; i < this.listaLineasNodos.length; ++i)
		this.dibujarArista(this.listaLineasNodos[i]);
}

LienzoCliente.prototype.dibujarArista = function(linea)
{
	this.context.beginPath();
	this.context.moveTo(linea.x1, linea.y1);
	this.context.lineTo(linea.x2, linea.y2);
	this.context.strokeStyle = "#CA3";
	this.context.lineWidth = 2;
	this.context.stroke();
	this.context.closePath();
}

LienzoCliente.prototype.dibujarFiguraConNodos = function( nodos )
{
	this.nodosFigura = [];
	this.listaLineasNodos = [];
	var self = this;
	nodos.forEach(function( nodo, index )
	{
		let punto = new Nodo(nodo.x / 2, nodo.y / 2);
		punto.radio = 3;
		self.nodosFigura.push( punto );
		if(index <= 0) return;
		self.formarLineasEntreNodos( index - 1, index );
	});

	this.formarLineasEntreNodos( 0, this.nodosFigura.length - 1);
}

LienzoCliente.prototype.formarLineasEntreNodos = function( indexUno, indexDos )
{
	var nodoUno = this.nodosFigura[ indexUno ];
	var nodoDos = this.nodosFigura[ indexDos ];

	var lineaNueva = new Linea(nodoUno.x, nodoUno.y, nodoDos.x, nodoDos.y);
	this.listaLineasNodos.push(lineaNueva);
};

LienzoCliente.prototype.dibujarNodoMomentaneo = function( nodo )
{
	console.log("Dibujar nodo: ", nodo);
	let objNodo = { }
	objNodo.tiempoInicial = Date.now()
	objNodo.pintar = true;
	objNodo.nodo = new Nodo(nodo.x / 2, nodo.y / 2)
	objNodo.nodo.radio = 2;
	this.nodosAEliminar.push( objNodo )
}

LienzoCliente.prototype.dibujarFlashEnPantalla = function( dt )
{
	if(this.flash.valor)
	{
		var tiempoEnDibujado = Date.now() - this.flash.tiempo;
		if(tiempoEnDibujado > 200)
		{
			this.flash.valor = false;
			this.flash.tiempo = 0;
			return;
		}

		this.context.beginPath();
		this.context.rect(0, 0, this.width, this.height);
		this.context.fillStyle = "#999";
		this.context.fill();
		this.context.closePath();
	}
}

LienzoCliente.prototype.limpiarNodosYCanvas = function( )
{
	this.nodosFigura = [];
	this.nodosAEliminar = [];
	this.listaLineasNodos = [];
	this.flash = {valor: true, tiempo: Date.now()};
}

LienzoCliente.prototype.tick = function( dt )
{
	this.actualizarDibujado( dt );
}

var FABRICA = FABRICA || {};
FABRICA.listaClientes = [];
FABRICA.indexAResponder = -1;

FABRICA.OnLienzoClienteClick = evento =>
{
	let index = Number(evento.target.getAttribute('id').split('_')[1]);
	FABRICA.indexAResponder = index;
	FABRICA.listaClientes.forEach(function( clienteObj )
	{
		console.log(clienteObj);
		if(clienteObj.index === index)
			clienteObj.lienzo.setActivo( true )
		else
			clienteObj.lienzo.setActivo( false )
	})
}

FABRICA.revisarSiElClienteEsNuevo = index =>
{
	let yaSeHaRegistrado = false;
	FABRICA.listaClientes.forEach(function( clienteObj )
	{
		if(clienteObj.index === index)
			yaSeHaRegistrado = true;
	})

	if(!yaSeHaRegistrado)
		FABRICA.registrarNuevoCliente(index)
}

FABRICA.registrarNuevoCliente = index =>
{
	var clienteObj = {}
	clienteObj.index = index;
	clienteObj.lienzo = new LienzoCliente(320, 320, index);
	FABRICA.listaClientes.push(clienteObj);
}

FABRICA.recibirObjetoPorParteDelCliente = (objeto, index) =>
{
	if(objeto.terminado)
		FABRICA.procesarObjetoTerminado( objeto, index );
	else
		FABRICA.procesarObjetoNoTerminado( objeto, index );
}

FABRICA.procesarObjetoTerminado = (objeto, index) =>
{
	let lienzo = FABRICA.encontrarLienzoClienteConIndex( index );
	lienzo.dibujarFiguraConNodos(objeto.nodos);
}

FABRICA.procesarObjetoNoTerminado = (objeto, index) =>
{
	let lienzo = FABRICA.encontrarLienzoClienteConIndex( index );
	lienzo.dibujarNodoMomentaneo(objeto.nodo);
}

FABRICA.encontrarLienzoClienteConIndex = index =>
{
	var lienzo = null;
	FABRICA.listaClientes.forEach(function( clienteObj )
	{
		if(clienteObj.index === index)
			lienzo = clienteObj.lienzo;
	})

	return lienzo;
}

FABRICA.responderFigura = ( ) =>
{
	console.log(FABRICA.indexAResponder)
	let lienzo = FABRICA.encontrarLienzoClienteConIndex( FABRICA.indexAResponder )
	JUEGO.canvas.compararNodosMandadosConLosRecibidos( lienzo.nodosFigura  );
	console.log(lienzo)
	lienzo.limpiarNodosYCanvas( );
}

FABRICA.tick = deltaTime =>
{
	FABRICA.listaClientes.forEach(function( clienteObj )
	{
		clienteObj.lienzo.tick( deltaTime );
	})
}

var RANDOM = RANDOM || {};

RANDOM.NuevaSenial = function()
{
	JUEGO.senial.nuevaSenial();
};

RANDOM.obtenerNuevo = function(inicio, final)
{
	return Math.floor((Math.random() * inicio) + final);
};

function procesarFiguraRecibida(data)
{
	JUEGO.canvas.procesarFiguraRecibida(data);
}

function probarFiguraRecibidaFabrica(datos)
{
	JUEGO.canvas.agregarFiguraRecibidaFabrica(datos);
}

JUEGO.obtenerNodoAMandar = (x, y) =>
{
	let objeto = { };
	objeto.terminado = false;
	objeto.nodo = new Nodo(x, y)
	return objeto;
}

JUEGO.obtenerObjetoAMandar = () =>
{
	let objeto = {}
	objeto.terminado = true;
	objeto.nodos = JUEGO.canvas.nodos;
	JUEGO.canvas.figuraMandada = objeto.nodos;
	return objeto;
}

JUEGO.revisarSiHaAtrapadoUnaGallina = obj =>
{
	JUEGO.gallinas.forEach(function(gallina)
	{
		var dist = JUEGO.canvas.obtenerDistanciaEntrePuntos(obj.nodo.x, obj.nodo.y, gallina.deltaX, gallina.deltaY);
		if(dist <= 40)
		{
			gallina.iniciarCongelacion();
		}
	});
}

JUEGO.activarGallinero = () =>
{
	JUEGO.formarUnaNuevaGallina();
	JUEGO.formarUnaNuevaGallina();
	JUEGO.formarUnaNuevaGallina();
	JUEGO.tiempoAnterior = Date.now();
	JUEGO.activarGallinas = true;
}

var Canvas = {

	Init: (tipoJugador) =>
	{
	    let canvas = new Lienzo(512, 512)
		Initialize(tipoJugador)
	},
	mandarFigura: evento =>
	{
		JUEGO.interfaz.mandarButtonClick()
		JUEGO.deAquiSalioLaFigura = true
	},

	obtenerNodoAMandar: (x, y) =>
	{
		return JUEGO.obtenerNodoAMandar( x, y );
	},

	obtenerObjetoAMandar: terminado =>
	{
		return JUEGO.obtenerObjetoAMandar( );
	},

	obtenerIndexAResponder: () =>
	{
		return FABRICA.indexAResponder;
	},

	figuraRecibida: datos =>
	{
		procesarFiguraRecibida(datos)
	},

	figuraRecibidaFabrica: (index, objeto) =>
	{
		FABRICA.revisarSiElClienteEsNuevo( index );
		FABRICA.recibirObjetoPorParteDelCliente( objeto, index );
		//probarFiguraRecibidaFabrica( datos );
	},

	activar: ( ) =>
	{
		JUEGO.canvas.setBackgroundActivo();
		if(tipoJugador === "cliente")
			JUEGO.activarGallinero();
	},

	responderFigura: ( ) =>
	{
		FABRICA.responderFigura();
	},

	revisarSiHaAtrapadoUnaGallina: obj =>
	{
		JUEGO.revisarSiHaAtrapadoUnaGallina( obj );
	},

	noSeHaCompletadoLaFigura: ( ) =>
	{
		console.log("Figura a completar");
		return !JUEGO.interfaz.revisarSiSeCompletoFigura();
	}


};

module.exports = {
  canvas: Canvas
};
