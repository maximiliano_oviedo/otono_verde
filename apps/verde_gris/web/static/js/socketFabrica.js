// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "web/static/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/my_app/endpoint.ex":
import {Socket} from "phoenix"
import logica from "./canvas"
logica.canvas.Init( "fabrica" )

let socket = new Socket("/socket", {params: {token: window.userToken}})

socket.connect()
//let channel           = socket.channel("game_data:lobby", {})
let channel           = socket.channel("game_transmision:fabrica", { })
let canvas            = $("#canvasContainer")

$("#interfaz").on("click", "#conexionButton", evento =>
{
  channel.push("transmision:iniciar_conexion_fabrica", { })
})

$("#interfaz").on("click", "#mandarButton", evento =>
{
  console.log('On mandar');
  //channel.push("figura-for-response", "nueva figura")

  let nodos = logica.canvas.obtenerNodosAMandar()
  logica.canvas.mandarFigura()
  let data = {nodos: nodos}
  // Se tiene que mandar siempre un objeto como el valor de la llave "objeto"
  channel.push("transmision:transmitir_objeto", {"objeto": data})
  //channel.push("counter:increment")
})

$("#interfaz").on("click", "#responderButton", evento =>
{
  //channel.push("figura-for-response", "nueva figura")

  let nodos = logica.canvas.obtenerNodosAMandar()
  let ultimoIndex = logica.canvas.obtenerUltimoIndex();
  logica.canvas.mandarFigura()
  let data = {nodos: nodos}
  console.log("On responder", ultimoIndex);
  // Se tiene que mandar siempre un objeto como el valor de la llave "objeto"
  channel.push("transmision:responder_transmision_objeto", {"index": ultimoIndex, "objeto": data})
  //channel.push("counter:increment")
})

/// Channel events

channel.on("on_conectado", msg => {
    logica.canvas.activar();
})

channel.on("user:entered", msg => {
  console.log("Usuario nuevo: ", msg);
})

channel.on('phx_reply', payload =>
{
  let response = payload["response"]
  if (response.length <= 0)
    return
})

channel.on('figura-data', payload =>
{
  if (payload != undefined)
  {
    logica.canvas.figuraRecibida(payload.nodos)
  }
})

channel.on('shout', payload =>
{
  if (payload != undefined)
  {
      console.log("Shout, ", payload.objeto);
      logica.canvas.figuraRecibida(payload.objeto)
  }

})

// broadcast
channel.on('objeto_fabrica', payload =>
{
  if (payload != undefined)
  {
      console.log("Payload: ", payload);
      //logica.canvas.figuraRecibidaFabrica(payload.index, payload.objeto.nodos)
  }

})

console.log("Socket: Fabrica");

// Now that you are connected, you can join channels with a topic:
//let channel = socket.channel("game_data:lobby", {})
channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
