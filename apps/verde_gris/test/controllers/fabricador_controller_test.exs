defmodule VerdeGris.FabricadorControllerTest do
  use VerdeGris.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/fabricador"
    assert html_response(conn, 200) =~ "Fabricador"
  end
end
