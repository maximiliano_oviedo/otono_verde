defmodule VerdeGris.GeomaroControllerTest do
  use VerdeGris.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/geomaro"
    assert html_response(conn, 200) =~ "Geomaro"
  end
end
