defmodule VerdeGris.PageControllerTest do
  use VerdeGris.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Verde con Gris"
  end
end
