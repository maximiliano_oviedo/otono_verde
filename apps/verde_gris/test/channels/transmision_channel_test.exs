defmodule VerdeGris.TransmisionChannelTest do
  use VerdeGris.ChannelCase
  alias VerdeGris.TransmisionChannel
  import TestHelper

  setup do
      id = new_id()
      {:ok, _, socket} =
        socket("user_id", %{})
        |> subscribe_and_join(TransmisionChannel, "game_transmision:#{id}")

      {:ok, socket: socket, id: id}
  end

  test "iniciar conexion con transmision para comenzar a jugar como cliente", %{socket: socket} do
      ref = push socket, "transmision:iniciar_conexion_cliente", %{ }
      assert_reply ref, :ok, %{conectado: true}
  end

  test "iniciar conexion con transmision y recibir un mensaje de conectado", %{socket: socket} do
      ref = push socket, "transmision:iniciar_conexion_cliente", %{ }
      assert_reply ref, :ok, %{conectado: true}
      assert_broadcast "on_conectado", %{"conectado" => true}
  end

  test "iniciar conexion con transmision para comenzar a jugar como fabrica", %{socket: socket} do
      ref = push socket, "transmision:iniciar_conexion_fabrica", %{ }
      assert_reply ref, :ok, %{conectado: true}
      assert_broadcast "on_conectado", %{"conectado" => true}
  end

  #@tag :skip
  test "mandar un objeto hacia la fabrica por parte de un cliente y verificar que se haya mandado", %{socket: socket} do
      push socket, "transmision:iniciar_conexion_cliente", %{ }
      push socket, "transmision:iniciar_conexion_fabrica", %{ }

      push socket, "transmision:transmitir_objeto", %{"objeto" => %{"x" => 121, "y" => 125}}

      ref = push socket, "prueba:probar_objeto_recibido_fabrica", %{}
      assert_reply ref, :ok, %{transmision: true}
  end

  #@tag :skip
  test "mandar un objeto hacia la fabrica y verificar que el cliente lo reciba", %{socket: socket} do
      push socket, "transmision:iniciar_conexion_cliente", %{ }
      push socket, "transmision:iniciar_conexion_fabrica", %{ }

      push socket, "transmision:transmitir_objeto", %{"objeto" => %{"x" => 121, "y" => 125}}
      push socket, "transmision:responder_transmision_objeto", %{"index" => 0, "objeto" => %{"x" => 121, "y" => 125}}

      ref = push socket, "prueba:probar_objeto_respondido_en_cliente", %{ }

      assert_reply ref, :ok, %{respuesta: true}

  end

  #@tag :skip
  test "mandar un objeto hacia la fabrica y responder, verificar objeto respondido en cliente", %{socket: socket} do
      push socket, "transmision:iniciar_conexion_cliente", %{nombre: :cliente_uno}
      push socket, "transmision:iniciar_conexion_fabrica", %{}

      push socket, "transmision:transmitir_objeto", %{"objeto" => %{"x" => 121, "y" => 125}}
      push socket, "transmision:responder_transmision_objeto", %{"index" => 0, "objeto" => %{"x" => 122, "y" => 125}}

      assert_broadcast "objeto", %{"x" => 122, "y" => 125}
  end

end
