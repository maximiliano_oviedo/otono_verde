defmodule MecanismoTransmision.Fabrica do

    @fabrica_agent :fabrica_agent

    def iniciar_conexion(scope) do
        Agent.start_link(fn -> {scope, :msgs, []} end, name: @fabrica_agent)
        MecanismoTransmision.Transmision.registrar_fabrica( @fabrica_agent )
    end

    def verificar_conexion do
        MecanismoTransmision.Transmision.la_fabrica_se_ha_registrado( @fabrica_agent )
        |> conexion_iniciada
    end

    def conexion_iniciada([_nombre_fabrica]) do
        {:ok, :conectado}
    end

    def conexion_iniciada() do
        {:error, :sin_conexion}
    end

    def obtener_transaccion_generada() do
        Agent.get(@fabrica_agent, fn tupla ->
            {_scope, :msgs, lista} = tupla
            lista
        end)
    end

    def responder_transaccion_hacia_el_cliente(index_cliente, objeto) do
        MecanismoTransmision.Transmision.iniciar_nueva_respuesta_hacia_el_cliente(index_cliente, objeto)
        |> transaccion_respondida
    end

    def transaccion_respondida({true, _msg}) do
        {:ok, :se_ha_respondido}
    end

    def transaccion_respondida({false, _msg}) do
        {:error, :no_se_respondio}
    end

    def nueva_transaccion(index_cliente, nombre, objeto) do
        Agent.update(@fabrica_agent, fn tupla ->
            {scope, :msgs, lista} = tupla
            scope.broadcast_change_fabrica( index_cliente, objeto )

            {scope, :msgs, lista ++ [{index_cliente, objeto}]}
        end)
    end

end
