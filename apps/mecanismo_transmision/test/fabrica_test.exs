defmodule MecanismoTransmision.FabricaTest do
  use ExUnit.Case, async: true

  @doctest """
    Estas pruebas estan hechas para probar una fabrica del modo mas sencillo y primitivo
    que se pueda, es un mecanismo donde la fabrica todo el tiempo esta esperando
  """

  test "conectar una sola fabrica" do
      MecanismoTransmision.Fabrica.iniciar_conexion(self)
      assert {:ok, :conectado} = MecanismoTransmision.Fabrica.verificar_conexion()
  end

  test "conectar fabrica por primera vez y volver a conectarlo, ver que no se duplique" do
      MecanismoTransmision.Fabrica.iniciar_conexion(self)
      assert {:ok, :conectado} = MecanismoTransmision.Fabrica.verificar_conexion()

      MecanismoTransmision.Fabrica.iniciar_conexion(self)
      assert {:ok, :conectado} = MecanismoTransmision.Fabrica.verificar_conexion()
  end

  test "iniciar una nueva transaccion por parte del cliente y generar una respuesta" do
      MecanismoTransmision.Fabrica.iniciar_conexion(self)

      MecanismoTransmision.Cliente.iniciar_conexion(:cliente_uno, self)
      MecanismoTransmision.Cliente.iniciar_transaccion_con_fabrica(:cliente_uno, {:vertices, x: 121, y: 125})

      assert [{_cliente, {:vertices, x: 121, y: 125}}] = MecanismoTransmision.Fabrica.obtener_transaccion_generada()
  end

  test "responder por parte de la fabrica a una transaccion iniciada por un cliente" do
      MecanismoTransmision.Fabrica.iniciar_conexion(self)

      MecanismoTransmision.Cliente.iniciar_conexion(:cliente_uno, self)
      MecanismoTransmision.Cliente.iniciar_transaccion_con_fabrica(:cliente_uno, {:vertices, x: 121, y: 125})

      [{index_cliente, {:vertices, x: x, y: y}}] = MecanismoTransmision.Fabrica.obtener_transaccion_generada()
      assert {:ok, :se_ha_respondido} = MecanismoTransmision.Fabrica.responder_transaccion_hacia_el_cliente(index_cliente, {:vertices, x: x + 10, y: y + 10})
  end

  test "responder por parte de la fabrica a una transaccion iniciada por dos clientes diferentes" do
      MecanismoTransmision.Fabrica.iniciar_conexion(self)

      MecanismoTransmision.Cliente.iniciar_conexion(:cliente_uno, self)
      MecanismoTransmision.Cliente.iniciar_conexion(:cliente_dos, self)

      MecanismoTransmision.Cliente.iniciar_transaccion_con_fabrica(:cliente_uno, {:vertices, x: 121, y: 125})
      [{index_cliente, {:vertices, x: x, y: y}}] = MecanismoTransmision.Fabrica.obtener_transaccion_generada()
      assert {:ok, :se_ha_respondido} = MecanismoTransmision.Fabrica.responder_transaccion_hacia_el_cliente(index_cliente, {:vertices, x: x + 10, y: y + 10})

      MecanismoTransmision.Cliente.iniciar_transaccion_con_fabrica(:cliente_dos, {:vertices, x: 10, y: 10})
      [_cliente_uno, {index_cliente_dos, {:vertices, x: x_dos, y: y_dos}}] = MecanismoTransmision.Fabrica.obtener_transaccion_generada()
      assert {:ok, :se_ha_respondido} = MecanismoTransmision.Fabrica.responder_transaccion_hacia_el_cliente(index_cliente_dos, {:vertices, x: x_dos + 10, y: y_dos + 10})
  end

  test "iniciar nueva transaccion por parte del cliente para transmitirlo hacia la fabrica y hacia un proceso extero" do
      MecanismoTransmision.Fabrica.iniciar_conexion(self)

      MecanismoTransmision.Cliente.iniciar_conexion(:cliente_uno, self)
      MecanismoTransmision.Cliente.iniciar_transaccion_con_fabrica(:cliente_uno, {:vertices, x: 121, y: 125})

      assert_receive {:vertices, x: 121, y: 125}
  end

end
